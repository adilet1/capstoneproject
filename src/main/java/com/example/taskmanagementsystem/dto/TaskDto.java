package com.example.taskmanagementsystem.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
public class TaskDto {
    private Long id;
    private String title;
    private String description;
    private Date dueDate;
    private String status;
    private String priority;
    private String creator;
    private List<String> assignees;
}

package com.example.taskmanagementsystem.repository;

import com.example.taskmanagementsystem.model.Task;
import com.example.taskmanagementsystem.model.Users;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
    Page<Task> findAllByCreator(Users user, Pageable pageable);
    Optional<Task> findTaskById(Long id);
    @Query("SELECT t FROM Task t WHERE t.creator = :user OR :user MEMBER OF t.assignedUsers")
    Page<Task> findAllCreatedAndAssignedTasks(@Param("user") Users user, Pageable pageable);
}

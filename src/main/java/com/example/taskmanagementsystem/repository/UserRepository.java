package com.example.taskmanagementsystem.repository;

import com.example.taskmanagementsystem.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface UserRepository extends JpaRepository<Users, Long> {

    Optional<Users> findByEmail(String email);
    boolean existsByEmail(String email);
    Set<Users> findAllByUsernameIn(List<String> usernames);
}

package com.example.taskmanagementsystem.config;

import com.example.taskmanagementsystem.model.Roles;
import com.example.taskmanagementsystem.model.Users;
import com.example.taskmanagementsystem.service.RoleService;
import com.example.taskmanagementsystem.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class CustomCommandLineRunner implements CommandLineRunner {


    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final RoleService roleService;

    @Autowired
    public CustomCommandLineRunner(
            UserService userService,
            PasswordEncoder passwordEncoder,
            RoleService roleService) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
        this.roleService = roleService;
    }

    @Override
    public void run(String...args) throws Exception {
        if (!userService.existsByEmail("admin@mail.com")) {
            Roles roleAdmin = roleService.getRoleById(1L);
            Set<Roles> adminRoles = new HashSet<>();
            adminRoles.add(roleAdmin);
            Users admin = new Users();
            admin.setUsername("admin");
            admin.setPassword(passwordEncoder.encode("admin"));
            admin.setRoles(adminRoles);
            admin.setEmail("admin@mail.com");
            userService.save(admin);
        }
    }
}

package com.example.taskmanagementsystem.model;

public enum Priority {
    LOW, MIDDLE, HIGH
}

package com.example.taskmanagementsystem.model;

public enum Status {
    NEW, RUNNING, DONE
}

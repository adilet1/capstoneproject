package com.example.taskmanagementsystem.rest;

import com.example.taskmanagementsystem.dto.TaskDto;
import com.example.taskmanagementsystem.model.Priority;
import com.example.taskmanagementsystem.model.Status;
import com.example.taskmanagementsystem.model.Task;
import com.example.taskmanagementsystem.model.Users;
import com.example.taskmanagementsystem.service.TaskService;
import com.example.taskmanagementsystem.service.UserService;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/task")
public class TaskRestController {

    private final UserService userService;
    private final TaskService taskService;

    public TaskRestController(UserService userService, TaskService taskService) {
        this.userService = userService;
        this.taskService = taskService;
    }

    @GetMapping()
    public Page<TaskDto> getAllTasks(@RequestParam(required = false) Integer page,
                                  @RequestParam(required = false, defaultValue = "1") Integer size) {
        Page<Task> tasks = taskService.findAllTasks(page, size);
        return tasks.map(task -> new TaskDto(
                task.getId(),
                task.getTitle(),
                task.getDescription(),
                task.getDueDate(),
                task.getPriority().name(),
                task.getStatus().name(),
                task.getCreator().getUsername(),
                task.getAssignedUsers().stream().map(Users::getUsername).toList()));
    }

    @PostMapping("/add")
    public ResponseEntity<Task> addTask(@RequestBody TaskDto taskDto) {
        Task task = new Task();
        task.setTitle(taskDto.getTitle());
        task.setDescription(taskDto.getDescription());
        task.setStatus(Status.NEW);
        task.setCreator(getUserData());
        task.setPriority(Priority.valueOf(taskDto.getPriority()));
        task.setDueDate(taskDto.getDueDate());
        task.setAssignedUsers(getAssignedUsers(taskDto.getAssignees()));
        return new ResponseEntity<>(taskService.save(task), HttpStatus.CREATED);
    }

    @GetMapping("/own")
    public Page<TaskDto> getAllOwnTasks(@RequestParam(required = false) Integer page,
                                     @RequestParam(required = false, defaultValue = "1") Integer size) {
        Page<Task> tasks;
        if (getUserData().isAdmin()) {
            tasks = taskService.findAllTasks(page, size);
        } else {
            tasks = taskService.findAllTasksByUserOrAssignedUsers(getUserData(), page, size);
        }
        return tasks.map(task -> new TaskDto(
                task.getId(),
                task.getTitle(),
                task.getDescription(),
                task.getDueDate(),
                task.getPriority().name(),
                task.getStatus().name(),
                task.getCreator().getUsername(),
                task.getAssignedUsers().stream().map(Users::getUsername).toList()));
    }

    @PutMapping("/{id}")
    public void editTask(@PathVariable Long id, @RequestBody TaskDto taskDto) {
        Task task = taskService.findTaskById(id).orElseThrow();
        task.setTitle(taskDto.getTitle());
        task.setDescription(taskDto.getDescription());
        task.setPriority(Priority.valueOf(taskDto.getPriority()));
        task.setStatus(Status.valueOf(taskDto.getStatus()));
        task.setDueDate(taskDto.getDueDate());
        task.setAssignedUsers(getAssignedUsers(taskDto.getAssignees()));
        taskService.save(task);
    }

    @DeleteMapping("/{id}")
    public void deleteTask(@PathVariable Long id) {
        taskService.deleteTaskById(id);
    }

    private Users getUserData(){
        Users userData = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(!(authentication instanceof AnonymousAuthenticationToken)){
            User secUser = (User) authentication.getPrincipal();
            userData = userService.findByEmail(secUser.getUsername());
        }
        return userData;
    }

    private Set<Users> getAssignedUsers(List<String> usernames) {
        Set<Users> assignedUsers = Collections.emptySet();
        if (usernames != null) {
            assignedUsers = userService.findAllByUsernameIn(usernames);
        }
        return assignedUsers;

    }

}

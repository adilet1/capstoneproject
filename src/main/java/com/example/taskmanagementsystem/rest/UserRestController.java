package com.example.taskmanagementsystem.rest;

import com.example.taskmanagementsystem.model.Users;
import com.example.taskmanagementsystem.service.UserService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserRestController {

    private final UserService userService;

    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping()
    public List<Users> getAllUsers() {
        return userService.findAll();
    }

}

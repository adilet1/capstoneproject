package com.example.taskmanagementsystem.service;

import com.example.taskmanagementsystem.model.Task;
import com.example.taskmanagementsystem.model.Users;
import org.springframework.data.domain.Page;

import java.util.Optional;

public interface TaskService {
    Page<Task> findAllTasks(Integer page, Integer size);
    Page<Task> findAllTasksByUser(Users user, Integer page, Integer size);
    Page<Task> findAllTasksByUserOrAssignedUsers(Users user, Integer page, Integer size);
    Optional<Task> findTaskById(Long id);
    Task save(Task task);
    void deleteTaskById(Long id);
}

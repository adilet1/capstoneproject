package com.example.taskmanagementsystem.service.impl;

import com.example.taskmanagementsystem.model.Task;
import com.example.taskmanagementsystem.model.Users;
import com.example.taskmanagementsystem.repository.TaskRepository;
import com.example.taskmanagementsystem.service.TaskService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;

    public TaskServiceImpl(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Page<Task> findAllTasks(Integer page, Integer size) {
        return taskRepository.findAll(getPageable(page, size));
    }

    @Override
    public Page<Task> findAllTasksByUser(Users user, Integer page, Integer size) {
        return taskRepository.findAllByCreator(user, getPageable(page, size));
    }

    @Override
    public Page<Task> findAllTasksByUserOrAssignedUsers(Users user, Integer page, Integer size) {
        return taskRepository.findAllCreatedAndAssignedTasks(user, getPageable(page, size));
    }

    @Override
    public Optional<Task> findTaskById(Long id) {
        return taskRepository.findTaskById(id);
    }

    @Override
    public Task save(Task task) {
        return taskRepository.save(task);
    }

    @Override
    public void deleteTaskById(Long id) {
        taskRepository.deleteById(id);
    }

    private Pageable getPageable(Integer page, Integer size) {
        if (page == null || size == null) {
            return Pageable.unpaged();
        }
        return PageRequest.of(page, size);
    }
}

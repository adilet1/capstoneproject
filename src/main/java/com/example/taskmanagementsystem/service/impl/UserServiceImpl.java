package com.example.taskmanagementsystem.service.impl;

import com.example.taskmanagementsystem.model.Users;
import com.example.taskmanagementsystem.repository.UserRepository;
import com.example.taskmanagementsystem.service.UserService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Users user = userRepository.findByEmail(username).orElseThrow();
        return new User(user.getEmail(), user.getPassword(), user.getRoles());
    }

    @Override
    public Users findByEmail(String email) {
        return userRepository.findByEmail(email).orElseThrow();
    }

    @Override
    public Set<Users> findAllByUsernameIn(List<String> usernames) {
        return userRepository.findAllByUsernameIn(usernames);
    }

    @Override
    public Users findById(Long id) {
        return userRepository.findById(id).orElseThrow();
    }

    @Override
    public boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    public List<Users> findAll() {
        return userRepository.findAll();
    }

    @Override
    public Users save(Users user) {
        return userRepository.save(user);
    }
}

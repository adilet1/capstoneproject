package com.example.taskmanagementsystem.service.impl;

import com.example.taskmanagementsystem.model.Roles;
import com.example.taskmanagementsystem.repository.RoleRepository;
import com.example.taskmanagementsystem.service.RoleService;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Roles getRoleById(long id) {
        return roleRepository.getReferenceById(id);
    }
}

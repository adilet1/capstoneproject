package com.example.taskmanagementsystem.service;

import com.example.taskmanagementsystem.model.Users;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;
import java.util.Set;

public interface UserService extends UserDetailsService {

    Users findByEmail(String email);
    Set<Users> findAllByUsernameIn(List<String> usernames);
    Users findById(Long id);
    boolean existsByEmail(String email);
    List<Users> findAll();
    Users save(Users user);

}

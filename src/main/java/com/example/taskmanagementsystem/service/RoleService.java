package com.example.taskmanagementsystem.service;

import com.example.taskmanagementsystem.model.Roles;

public interface RoleService {
    Roles getRoleById(long id);
}

package com.example.taskmanagementsystem.controller;

import com.example.taskmanagementsystem.dto.TaskDto;
import com.example.taskmanagementsystem.model.Task;
import com.example.taskmanagementsystem.model.Users;
import com.example.taskmanagementsystem.service.TaskService;
import com.example.taskmanagementsystem.service.UserService;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/task")
public class TaskController {

    private final UserService userService;
    private final TaskService taskService;

    public TaskController(UserService userService, TaskService taskService) {
        this.userService = userService;
        this.taskService = taskService;
    }

    @GetMapping("/{id}")
    public String getTaskById(@PathVariable Long id, Model model) {
        Task task = taskService.findTaskById(id).orElseThrow();
        TaskDto taskDto = new TaskDto(
                task.getId(),
                task.getTitle(),
                task.getDescription(),
                task.getDueDate(),
                task.getPriority().name(),
                task.getStatus().name(),
                task.getCreator().getUsername(),
                task.getAssignedUsers().stream().map(Users::getUsername).toList());
        model.addAttribute("task", taskDto);
        return "taskDetails";
    }

    @GetMapping("/own")
    public String getOwnTasks() {
        return "ownTasks";
    }

}

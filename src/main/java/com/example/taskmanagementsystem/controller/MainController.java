package com.example.taskmanagementsystem.controller;

import com.example.taskmanagementsystem.model.Roles;
import com.example.taskmanagementsystem.model.Users;
import com.example.taskmanagementsystem.service.RoleService;
import com.example.taskmanagementsystem.service.UserService;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Collections;

@Controller
public class MainController {

    private final UserService userService;
    private final RoleService roleService;
    private final PasswordEncoder passwordEncoder;

    public MainController(UserService userService, RoleService roleService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.roleService = roleService;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping(path = "/")
    public String index(Model model) {
        if (getUserData() !=null) {
            model.addAttribute("username", getUserData().getUsername());
        }
        return "index";
    }

    @GetMapping(path = "/login")
    public String login(Model model) {
        return "login";
    }

    @GetMapping(path = "/register")
    public String register(Model model) {
        return "register";
    }

    @PostMapping(path = "/signup")
    public String signUp(
            @RequestParam(name = "username") String username,
            @RequestParam(name = "email") String email,
            @RequestParam(name = "password1") String password1,
            @RequestParam(name = "password2") String password2,
            RedirectAttributes redirectAttrs
    ) {
        if (password1.equals(password2)) {
            if (!userService.existsByEmail(email)) {
                Roles userRole = roleService.getRoleById(2);
                Users user = new Users();
                user.setUsername(username);
                user.setEmail(email);
                user.setPassword(passwordEncoder.encode(password1));
                user.setRoles(Collections.singleton(userRole));
                userService.save(user);
                redirectAttrs.addFlashAttribute("success", true);
            } else {
                redirectAttrs.addFlashAttribute("notUniqueEmail", true);
            }
        } else {
            redirectAttrs.addFlashAttribute("passwordNotMatch", true);
        }
        return "redirect:/register";
    }

    private Users getUserData(){
        Users userData = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(!(authentication instanceof AnonymousAuthenticationToken)){
            User secUser = (User) authentication.getPrincipal();
            userData = userService.findByEmail(secUser.getUsername());
        }
        return userData;
    }
}

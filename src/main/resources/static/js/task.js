new Vue({
    el: '#app',
    data: {
        currentPage: 1,
        totalPages: 0,
        tasks: [],
        users: [],
        form: {
            title: '',
            description: '',
            dueDate: '',
            priority: '',
            assignees: []
        },
    },
    methods: {
        resetForm() {
            this.form = {
                title: '',
                description: '',
                dueDate: '',
                priority: '',
                assignees: []
            };
        },
        fetchTasks: function(page){
            let params = {
                params:{
                    page: page-1,
                    size: 6
                }
            };
            axios.get('http://localhost:8080/api/task', params).then(
                (response) => {
                    this.tasks = response.data.content;
                    this.tasks.forEach(formatDate);
                    function formatDate(task) {
                        task.dueDate = moment(new Date(task.dueDate)).format("YYYY/MM/DD HH:mm");
                    }
                    this.totalPages = parseInt(response.data.totalPages);
                    this.currentPage = page;
                }
            );
        },
        fetchUsers: function () {
            axios.get('http://localhost:8080/api/user').then(
                (response) => {
                    this.users = response.data;
                }
            );
        },
        addModal: function(){
            this.fetchUsers()
            $('#addModal').modal('show');
            this.resetForm();
        },
        addTask: function () {
            console.log(this.form.assignees[1]);
            axios.post('http://localhost:8080/api/task/add', {
                title: this.form.title,
                description: this.form.description,
                dueDate: this.form.dueDate,
                priority: this.form.priority,
                assignees: this.form.assignees
            }).then(() => {
                $('#addModal').modal('hide');
                this.fetchTasks(this.currentPage);
                this.resetForm();
            }).catch(() => {
                this.resetForm();
            })
        }
    },
    created: function () {
        this.fetchTasks(this.currentPage);
    }
});

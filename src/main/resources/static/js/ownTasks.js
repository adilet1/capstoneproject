new Vue({
    el: '#app',
    data: {
        currentPage: 1,
        totalPages: 0,
        tasks: [],
        users: [],
        form: {
            id: '',
            title: '',
            description: '',
            dueDate: '',
            status: '',
            priority: '',
            assignees: []
        },
    },
    methods: {
        resetForm() {
            this.form = {
                id: '',
                title: '',
                description: '',
                dueDate: '',
                priority: '',
                assignees: []
            };
        },
        fetchTasks: function(page){
            let params = {
                params:{
                    page: page-1,
                    size: 6
                }
            };
            axios.get('http://localhost:8080/api/task', params).then(
                (response) => {
                    this.tasks = response.data.content;
                    this.tasks.forEach(formatDate);
                    function formatDate(task) {
                        task.dueDate = moment(new Date(task.dueDate)).format("YYYY/MM/DD HH:mm");
                    }
                    this.totalPages = parseInt(response.data.totalPages);
                    this.currentPage = page;
                }
            );
        },
        editTask: function () {
            axios.put('http://localhost:8080/api/task/' + this.form.id, {
                title: this.form.title,
                description: this.form.description,
                dueDate: this.form.dueDate,
                status: this.form.status,
                priority: this.form.priority,
                assignees: this.form.assignees
            }).then(
                (response) => {
                    $('#editModal').modal('hide');
                    this.fetchTasks(this.currentPage);
                    this.resetForm()
                }
            );
        },
        fetchUsers: function () {
            axios.get('http://localhost:8080/api/user').then(
                (response) => {
                    this.users = response.data;
                }
            );
        },
        deleteTask: function () {
            axios.delete('http://localhost:8080/api/task/' + this.form.id).then(
                (response) => {
                    $('#deleteModal').modal('hide');
                    this.fetchTasks(this.currentPage);
                    this.resetForm()
                }
            );
        },
        deleteModal: function(task){
            $('#deleteModal').modal('show');
            this.form.id = task.id;
        },
        editModal: function(task){
            $('#editModal').modal('show');
            this.form.id = task.id;
            this.form.title = task.title;
            this.form.description = task.description;
            this.form.dueDate = task.dueDate;
            this.form.status = task.status;
            this.form.priority = task.priority;
            this.form.assignees = task.assignees;
        },
    },
    created: function () {
        this.fetchTasks(this.currentPage);
        this.fetchUsers();
    }
});

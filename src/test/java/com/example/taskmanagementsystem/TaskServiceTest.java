package com.example.taskmanagementsystem;

import com.example.taskmanagementsystem.model.Task;
import com.example.taskmanagementsystem.model.Users;
import com.example.taskmanagementsystem.repository.TaskRepository;
import com.example.taskmanagementsystem.service.impl.TaskServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TaskServiceTest {

    @Mock
    private TaskRepository taskRepository;

    @InjectMocks
    private TaskServiceImpl taskService;

    private Task task;
    private Users user;

    @BeforeEach
    public void setUp() {
        task = new Task();
        task.setId(1L);
        task.setTitle("Test Task");

        user = new Users();
        user.setId(1L);
        user.setEmail("user@example.com");
    }

    @Test
    void testFindAllTasks() {
        List<Task> tasks = Arrays.asList(task);
        Page<Task> taskPage = new PageImpl<>(tasks);
        Pageable pageable = PageRequest.of(0, 10);

        when(taskRepository.findAll(pageable)).thenReturn(taskPage);

        Page<Task> result = taskService.findAllTasks(0, 10);

        assertEquals(taskPage, result);
        verify(taskRepository, times(1)).findAll(pageable);
    }

    @Test
    void testFindAllTasksByUser() {
        List<Task> tasks = Arrays.asList(task);
        Page<Task> taskPage = new PageImpl<>(tasks);
        Pageable pageable = PageRequest.of(0, 10);

        when(taskRepository.findAllByCreator(user, pageable)).thenReturn(taskPage);

        Page<Task> result = taskService.findAllTasksByUser(user, 0, 10);

        assertEquals(taskPage, result);
        verify(taskRepository, times(1)).findAllByCreator(user, pageable);
    }

    @Test
    void testFindAllTasksByUserOrAssignedUsers() {
        List<Task> tasks = Arrays.asList(task);
        Page<Task> taskPage = new PageImpl<>(tasks);
        Pageable pageable = PageRequest.of(0, 10);

        when(taskRepository.findAllCreatedAndAssignedTasks(user, pageable)).thenReturn(taskPage);

        Page<Task> result = taskService.findAllTasksByUserOrAssignedUsers(user, 0, 10);

        assertEquals(taskPage, result);
        verify(taskRepository, times(1)).findAllCreatedAndAssignedTasks(user, pageable);
    }

    @Test
    void testFindTaskById() {
        when(taskRepository.findTaskById(1L)).thenReturn(Optional.of(task));

        Optional<Task> result = taskService.findTaskById(1L);

        assertTrue(result.isPresent());
        assertEquals(task, result.get());
        verify(taskRepository, times(1)).findTaskById(1L);
    }

    @Test
    void testSave() {
        when(taskRepository.save(task)).thenReturn(task);

        Task result = taskService.save(task);

        assertEquals(task, result);
        verify(taskRepository, times(1)).save(task);
    }

    @Test
    void testDeleteTaskById() {
        doNothing().when(taskRepository).deleteById(1L);

        taskService.deleteTaskById(1L);

        verify(taskRepository, times(1)).deleteById(1L);
    }
}

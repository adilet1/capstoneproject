package com.example.taskmanagementsystem;

import com.example.taskmanagementsystem.model.Users;
import com.example.taskmanagementsystem.repository.UserRepository;
import com.example.taskmanagementsystem.service.impl.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImpl userService;

    private Users user;

    @BeforeEach
    public void setUp() {
        user = new Users();
        user.setId(1L);
        user.setEmail("test@example.com");
        user.setPassword("password");
        user.setRoles(Collections.emptySet());
    }

    @Test
    void testLoadUserByUsername_UserFound() {
        when(userRepository.findByEmail(anyString())).thenReturn(Optional.of(user));

        UserDetails userDetails = userService.loadUserByUsername("test@example.com");

        assertEquals(user.getEmail(), userDetails.getUsername());
        assertEquals(user.getPassword(), userDetails.getPassword());
        assertEquals(user.getRoles(), userDetails.getAuthorities());
    }

    @Test
    void testLoadUserByUsername_UserNotFound() {
        when(userRepository.findByEmail(anyString())).thenReturn(Optional.empty());

        assertThrows(NoSuchElementException.class, () -> userService.loadUserByUsername("test@example.com"));
    }

    @Test
    void testFindByEmail_UserFound() {
        when(userRepository.findByEmail(anyString())).thenReturn(Optional.of(user));

        Users foundUser = userService.findByEmail("test@example.com");

        assertEquals(user, foundUser);
    }

    @Test
    void testFindByEmail_UserNotFound() {
        when(userRepository.findByEmail(anyString())).thenReturn(Optional.empty());

        assertThrows(NoSuchElementException.class, () -> userService.findByEmail("test@example.com"));
    }

    @Test
    void testFindAllByUsernameIn() {
        Set<Users> usersSet = new HashSet<>();
        usersSet.add(user);
        when(userRepository.findAllByUsernameIn(anyList())).thenReturn(usersSet);

        Set<Users> foundUsers = userService.findAllByUsernameIn(Collections.singletonList("test@example.com"));

        assertEquals(usersSet, foundUsers);
    }

    @Test
    void testFindById_UserFound() {
        when(userRepository.findById(anyLong())).thenReturn(Optional.of(user));

        Users foundUser = userService.findById(1L);

        assertEquals(user, foundUser);
    }

    @Test
    void testFindById_UserNotFound() {
        when(userRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(NoSuchElementException.class, () -> userService.findById(1L));
    }

    @Test
    void testExistsByEmail_UserExists() {
        when(userRepository.existsByEmail(anyString())).thenReturn(true);

        assertTrue(userService.existsByEmail("test@example.com"));
    }

    @Test
    void testExistsByEmail_UserDoesNotExist() {
        when(userRepository.existsByEmail(anyString())).thenReturn(false);

        assertFalse(userService.existsByEmail("test@example.com"));
    }

    @Test
    void testFindAll() {
        List<Users> usersList = Collections.singletonList(user);
        when(userRepository.findAll()).thenReturn(usersList);

        List<Users> foundUsers = userService.findAll();

        assertEquals(usersList, foundUsers);
    }

    @Test
    void testSave() {
        when(userRepository.save(any(Users.class))).thenReturn(user);

        Users savedUser = userService.save(user);

        assertEquals(user, savedUser);
    }
}


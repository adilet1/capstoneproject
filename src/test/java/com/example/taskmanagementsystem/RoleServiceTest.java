package com.example.taskmanagementsystem;

import com.example.taskmanagementsystem.model.Roles;
import com.example.taskmanagementsystem.repository.RoleRepository;
import com.example.taskmanagementsystem.service.impl.RoleServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RoleServiceTest {

    @Mock
    private RoleRepository roleRepository;

    @InjectMocks
    private RoleServiceImpl roleService;

    private Roles role;

    @BeforeEach
    public void setUp() {
        role = new Roles();
        role.setId(1L);
        role.setRole("ROLE_ADMIN");
    }

    @Test
    void testGetRoleById_RoleFound() {
        when(roleRepository.getReferenceById(1L)).thenReturn(role);

        Roles foundRole = roleService.getRoleById(1L);

        assertEquals(role, foundRole);
        verify(roleRepository, times(1)).getReferenceById(1L);
    }

    @Test
    void testGetRoleById_RoleNotFound() {
        when(roleRepository.getReferenceById(3L)).thenReturn(null);

        Roles foundRole = roleService.getRoleById(3L);

        assertNull(foundRole);
        verify(roleRepository, times(1)).getReferenceById(3L);
    }

}

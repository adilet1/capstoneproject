# CapstoneProject

I am going to create web-site for 'Task management system'.
1. Not authorized user can see tasks. 
2. User can authorize. 
3. User can create a new task and assign it to other user. 
4. Admin has full access to edit/delete/create tasks. 
5. User can edit details of their tasks.
Git link: https://gitlab.com/adilet1/capstoneproject
